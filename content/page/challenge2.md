Naam: ............................................................

|  | Opdracht | Stempel |
| ------ | ------ | ------ |
| 1. | Laat je robot rijden: van voor naar achter van links naar rechts! Gebruik de pijltjes op de computer en vergeet de noodstop knop niet! Tip: gebruik zowel de 'wanneer \[pijl omhoog\] toets is ingedrukt' als 'wanneer \[pijl omhoog\] toets is losgelaten' | |
| 2. | Sluit een USB-controller aan. Programmeer deze zo dat de joystick de pijltjes van jouw computer overneemt. | |
| 3. | Elke robot heeft een nummerplaat (schermpje): gebruik de initialen van je naam of een leuke tekening om jouw robot herkenbaar te maken | |
| 4. | Gebruik de RGB (RoodGroenBlauw) leds vooraan als richtingaanwijzers, zodat iedereen kan zien dat je naar links of rechts rijdt. Tip: meng Rood en Groen om geel te krijgen | |
| 5. | Heb je al eens een vrachtwagen horen piepen als hij achteruit rijdt? Doe hetzelfde met je robot en gebruik daarbij je zoemer | |
| 6. | Als het donker wordt, moeten de rgb leds vooraan wit beginnen branden. Vergeet daarbij niet dat je richtingaanwijzers moeten kunnen blijven werken! Tip: laat de rgb leds niet te hard branden, anders denkt de sensor dat het weer licht is | |
| 7. | Laat de 4 leds achteraan telkens om de beurt geel branden zoals een seinlicht. | |
| 8. | Je motor mag pas beginnen werken als je eerst de startknop (drukknop) op de robot hebt geduwd. Tip: gebruik een variabele die gezet wordt door je drukknop | |
| 9. | Maak van je robot een ziekenwagen: laat de RGB leds vooraan om de beurt blauw knipperen en een sirene nadoen. | |
| 10. | Zorg ervoor dat je robot op verschillende snelheden kan rijden: van toets 1 als traagste tot toets 9 als snelste (zoals een versnellingsbak van een auto). Je moet nog steeds kunnen sturen met de pijltjes, enkel de snelheid wordt ingesteld met de nummertoetsen. Tip: gebruik variabelen. | |
